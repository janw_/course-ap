/** @elements : contains elements of the type E
 *  @structure : no structure
 *  @domain : all elements of the type E
 *  @constructor - Set();
 * <dl>
 *    <dt><b>PRE-condition</b><dd>      -
 *    <dt><b>POST-condition</b><dd>  The new Set object is an empty set.
 * </dl>
 **/

interface SetInterface<E extends Comparable> {


    /**
     * @precondition -
     * @postcondition - Initialize Set object with empty set.
     **/
    void init();


    /**
     * @precondition -
     * @postcondition - Type e is added to the end of the set.
     **/
    void add(E e);


    /**
     * @precondition -
     * @postcondition - Type e is removed from the set.
     **/
    void remove(E e);


    /**
     * @precondition -
     * @postcondition - Size of the set is returned as an int value.
     **/
    int size();


    /**
     * @precondition -
     * @postcondition - The current element is returned and removed from the set.
     **/
    E getElement();


    /**
     * @precondition -
     * @postcondition - True: set is empty
     *                  False: set is not empty
     **/
    boolean isEmpty();

    /**
     * @precondition -
     * @postcondition - True: set contains e
     *                  False: set does not contain e
     **/
    boolean contains(E e);

    /**
     * @parameter - A Set object
     * @precondition -
     * @postcondition - A new set object is returned that contains the union of the two sets.
     */
    Set<E> union(Set<E> set);


    /**
     * @parameter - A Set object
     * @precondition -
     * @postcondition - A new set object is returned that contains the intersection of the two sets.
     */
    Set<E> intersection(Set<E> set);


    /**
     * @parameter - A Set object
     * @precondition -
     * @postcondition - A new set object is returned that contains the complement of the two sets.
     */
    Set<E> complement(Set<E> set);


    /**
     * @parameter - A Set object
     * @precondition -
     * @postcondition - A new set object is returned that contains the symmetric difference of the two sets.
     */
    Set<E> symmetricDifference(Set<E> set);


    /**
     * @precondition -
     * @postcondition - A deep copy of the set is returned.
     */
    Set<E> copy();

}