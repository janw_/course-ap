/** @elements : alphanumeric characters of type char
 * @structure : linear
 * @domain : All rows of alphanumeric characters
 * @constructor - Identifier();
 * <dl>
 *    <dt><b>PRE-condition</b><dd>      -
 *    <dt><b>POST-condition</b><dd>  A row that starts with a letter and contains an undefined characters
 * </dl>
 **/

public interface IdentifierInterface {

//    /**
//     * @parameter - The String when Identifier is initialized.
//     * @precondition -
//     * @postcondition - Initialize Identifier object with char c.
//     **/
//    void init(String string);

    /**
     * @precondition -
     * @postcondition - The identifier is returned as a string.
     **/
    String toString();

    /**
     * @precondition -
     * @postcondition - True: source is equal to identifier
     *                  False: source is not equal to identifier
     **/
    boolean equals(Object source);

    /**
     * @precondition -
     * @postcondition - The identifier is returned as a hash
     **/
    int hashCode();

    /**
     * @precondition -
     * @postcondition - The identifier is returned as a string.
     **/
    int compareTo(Object source);


}