public class Set<E extends Comparable> implements SetInterface<E> {
    List<E> list;

    Set() {
        init();
    }

    public void init() {
        list = new List();
    }

    public void add(E e) {
        if (!list.find(e)) {
            list.insert(e);
        }
    }

    @Override
    public void remove(E e) {
        if (list.find(e)) {
            list.remove();
        }
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public E getElement() {
        E e = (E) list.retrieve();
        remove(e);

        return e;
    }

    public boolean isEmpty() {
        return list.size() == 0;
    }

    public boolean contains(E e) {
        return list.find(e);
    }

    @Override
    public Set<E> union(Set<E> set) {
        if (set.list.goToFirst()) {
            do {
                if (!list.find(set.list.retrieve())) {
                    list.insert(set.list.retrieve());
                }

            }   while (set.list.goToNext());
        }
        return this;
    }

    @Override
    public Set<E> intersection(Set<E> set) {
        Set<E> temp = new Set<>();

        if (set.list.goToFirst()) {
            do  {
                if (list.find(set.list.retrieve())) {
                    temp.add((E) set.list.retrieve());
                }
            } while (set.list.goToNext());
        }
        return temp;
    }

    @Override
    public Set<E> complement(Set<E> set) {
        Set<E> copy = set.copy();
        Set<E> temp = copy();

        while (!copy.isEmpty()) {
            temp.list.goToFirst();
            if (temp.contains(copy.list.retrieve())) temp.list.remove();
            copy.list.remove();
        }
        return temp;
    }

    @Override
    public Set<E> symmetricDifference(Set<E> set) {
        Set<E> temp1 = complement(set);
        Set<E> temp2 = set.complement(copy());
        Set<E> result = new Set<E>();

        if (temp1.list.goToFirst()) {
            do {
                result.add(temp1.list.retrieve());
            } while (temp1.list.goToNext());
        }

        if (temp2.list.goToFirst()) {
            do {
                result.add(temp2.list.retrieve());
            } while (temp2.list.goToNext());
        }

        return result;
    }

    @Override
    public Set<E> copy() {
        Set<E> copy =  new Set<E>();
        copy.init();
        copy.list = list == null ? null : (List<E>) list.copy();
        return copy;
    }
}