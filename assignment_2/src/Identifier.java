public class Identifier implements IdentifierInterface {

    private String identifier = "";

    public Identifier(Identifier source) {
        identifier = source.toString();
    }

    public Identifier(String identifier) {
        this.identifier = identifier;
    }

    public void init(String string) {
        identifier = string;
    }

    @Override
    public String toString() {
        return identifier;
    }

    public boolean equals(Object source) {
        return identifier.equals(((Identifier) source).toString());
    }

    public int hashCode(){
        return identifier.hashCode();
    }

    public int compareTo(Object source) {
        Identifier source2 = (Identifier) source;
        return toString().compareTo(source.toString());
    }

    public Identifier copy() {
        return new Identifier(this);
    }
}


//public class Identifier implements IdentifierInterface {
//
//    private StringBuffer identifier;
//
//    public Identifier(Identifier source) {
//        identifier.append(source);
//    }
//
//    public Identifier(StringBuffer identifier) {
//        this.identifier = identifier;
//    }
//
////    @Override
////    public void init(String string) {
////        identifier = string;
////    }
//
//    @Override
//    public String toString() {
//        return identifier.toString();
//    }
//
//    @Override
//    public boolean equals(Object source) {
//        return identifier.toString().equals(source.toString());
//    }
//
//    @Override
//    public int hashCode(){
//        return identifier.hashCode();
//    }
//
//    @Override
//    public int compareTo(Object source) {
//        Identifier source2 = (Identifier) source;
//        return toString().compareTo(source.toString());
//    }
//
////    @Override
////    public Identifier copy() {
////        return new Identifier(this);
////    }
//}
//
////    private String identifier = "";
////
////    public Identifier(Identifier source) {
////        identifier = source.toString();
////    }
////
////    public Identifier(String identifier) {
////        this.identifier = identifier;
////    }
////
////    @Override
////    public void init(String string) {
////        identifier = string;
////    }
////
////    @Override
////    public String toString() {
////        return identifier;
////    }
////
////    public boolean equals(Object source) {
////        return identifier.equals(((Identifier) source).toString());
////    }
////
////    public int hashCode(){
////        return identifier.hashCode();
////    }
////
////    public int compareTo(Object source) {
////        Identifier source2 = (Identifier) source;
////        return toString().compareTo(source.toString());
////    }
////
////    public Identifier copy() {
////        return new Identifier(this);
////    }
////}