# Course: Advanced Programming

Created at Vrije Universiteit Amsterdam

## Contributers

- [mwa220](https://www.github.com/mwa220)
- janw_

## Description

Assignment 1: A calculator which uses the shunting-yard algorithm.

Assignment 2: A calculator for sets which uses a Backus-Naur form parser.

## Usage

Use Apache Ant for building binaries: `ant jar`. For running use `ant run`.

## Tests

Use `ant test` for both assignments.

## Note

Test files found in `test` directories are property of Vrije Universiteit Amsterdam.
