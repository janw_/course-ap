public class TokenListC implements TokenList{

    private TokenC[] tokenArray;
    private int numberOfElements;
    public static final int INIT_SIZE = 10;

    public TokenListC(){
        tokenArray = new TokenC[INIT_SIZE];
        numberOfElements = 0;
    }

    public TokenListC(TokenListC source){
        numberOfElements = source.numberOfElements;
        copyElements(tokenArray, source.tokenArray, numberOfElements);
    }

    private void copyElements(TokenC[] list, TokenC[] list1, int numberOfElements) {
        for (int i = 0; i < numberOfElements; i++){
            list[i] = new TokenC(list1[i]);
        }
    }

    @Override
    public void add(Token token){
        if(numberOfElements == tokenArray.length - 1){
            doubleArray();
        }
        tokenArray[numberOfElements] = (TokenC) token;
        numberOfElements += 1;
    }

    @Override
    public void remove(int index){
        if (index > numberOfElements && index < 0){
            System.exit(0);
        }
        for (int i = index; i < numberOfElements - 1; i++){
            tokenArray[i] = tokenArray[i + 1];
        }
    }

    @Override
    public void set(int index, Token token){
        if (index > numberOfElements){
            System.exit(0);
        }
        for (int i = index; i < numberOfElements - 1; i++) {
            tokenArray[i + 1] = tokenArray[i];
        }
        tokenArray[index] = (TokenC) token;
    }

    @Override
    public Token get(int index){
        return tokenArray[index];
    }

    @Override
    public int size(){
        return numberOfElements;
    }

    private void doubleArray() {
        TokenC[] temp = new TokenC[2 * tokenArray.length];

        copyElements(temp, tokenArray, numberOfElements);
        tokenArray = temp;
    }
}
